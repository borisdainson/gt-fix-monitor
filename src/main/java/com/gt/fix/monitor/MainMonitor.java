package com.gt.fix.monitor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JFrame;

import org.apache.log4j.Logger;
import org.quickfixj.jmx.JmxExporter;

import com.gt.fix.monitor.ui.MonitorFrame;
import com.gt.util.EmailSender;

import quickfix.DefaultMessageFactory;
import quickfix.FileStoreFactory;
import quickfix.Initiator;
import quickfix.LogFactory;
import quickfix.MessageFactory;
import quickfix.MessageStoreFactory;
import quickfix.SLF4JLogFactory;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionSettings;
import quickfix.SocketInitiator;

public class MainMonitor {
    private static Logger log = Logger.getLogger(MainMonitor.class);
    private static MainMonitor mainMonitor;
    private boolean initiatorStarted = false;
    private Initiator initiator = null;
    private JFrame frame = null;
    private static final String DEFAULT_CONFIG_FILE_NAME = "/monitor.cfg";
	private static final long AVAILABILITY_CHECK_INTERVAL_MILLIS = 5*60*1000;
	private static final String MONITORED_KEYS_RESOURCE_NAME = "/monitored_fix_keys.txt";
	private static final String MONITORED_KEYS_OVERRIDE_FILE_NAME = "conf/monitored_fix_keys.txt";
	private static final String MARKET_SEGMENT_PROPERTIES_FILE_NAME = "conf/market_segment.properties";
	
	private static final String START_TIME_DEFAULT = "02:15:00";
	private static final String END_TIME_DEFAULT = "19:00:00";

	private String emailTo = "supportgroup@trueex.com,boris.dainson@gmail.com,support@greenwichtechnology.io";
		//"boris.dainson@gmail.com";
	private String emailSubject = "Alert from FIX Monitor";
	private String propertiesFile = "conf/fix-monitoring.txt";
	private long timeOfDayMillisInternalStart = Long.MAX_VALUE; 
	private long timeOfDayMillisStart = 0L; //parseTimeOfDay("6:30:00");
	private long timeOfDayMillisEnd = Long.MAX_VALUE; //parseTimeOfDay("19:00:00");
	private String header = "FIX data not available";
			
    private Timer timer = new Timer();
    private MonitorApplication application;
    
    public MainMonitor(String[] args) throws Exception {
        InputStream inputStream = null;
        if (args.length == 0) {
        	System.out.println("using default config from resource " + DEFAULT_CONFIG_FILE_NAME);
            inputStream = MainMonitor.class.getResourceAsStream(DEFAULT_CONFIG_FILE_NAME);
        } 
        else if (args.length == 1) {
        	String res = args[0];
        	if(res.toLowerCase().startsWith("file:")){
        		System.out.println("using file: " + res);	
        		inputStream = new FileInputStream(res.substring("file:".length()));
        	}
        	else{
        		System.out.println("using resource: " + res);
        		inputStream = MainMonitor.class.getResourceAsStream(res);
        	}
        }
        if (inputStream == null) {
            throw new NullPointerException("usage: " + MainMonitor.class.getName() + 
            		" configFilename or provide in the classpath default resource: " + DEFAULT_CONFIG_FILE_NAME);
        }
        SessionSettings settings = new SessionSettings(inputStream);
        inputStream.close();        
        
        OrderTableModel orderTableModel = new OrderTableModel();
        ExecutionTableModel executionTableModel = new ExecutionTableModel();
        application = new MonitorApplication(orderTableModel, executionTableModel);
        application.setMarketSegmentProperties(getMarketSegmentProperties());
        MessageStoreFactory messageStoreFactory = new FileStoreFactory(settings);
        //LogFactory logFactory = new ScreenLogFactory(true, true, true, true);
        LogFactory logFactory = new SLF4JLogFactory(settings);
        MessageFactory messageFactory = new DefaultMessageFactory();

        initiator = new SocketInitiator(application, messageStoreFactory, settings, logFactory,
                messageFactory);
       
        JmxExporter exporter = new JmxExporter();
        exporter.register(initiator);
        
        if(System.getProperty("MainMonitor.withUi", "false").equalsIgnoreCase("true")){
        	frame = new MonitorFrame(orderTableModel, executionTableModel, application);
        	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }

    private Properties getMarketSegmentProperties() throws IOException {		
		if(!new File(MARKET_SEGMENT_PROPERTIES_FILE_NAME).exists()){
			throw new FileNotFoundException("File not found: "+ MARKET_SEGMENT_PROPERTIES_FILE_NAME);
		}
	    try(InputStream is = new FileInputStream(MARKET_SEGMENT_PROPERTIES_FILE_NAME)) {
	    	Properties prop = new Properties();
	    	prop.load(is);
	    	log.info("getMarketSegmentProperties: " + prop);
	    	return prop;
		}
	}

	public synchronized void logon() {
        if (!initiatorStarted) {
            try {
                initiator.start();
                initiatorStarted = true;
            } catch (Exception e) {
                log.error("Logon failed", e);
            }
        } else {
            Iterator<SessionID> sessionIds = initiator.getSessions().iterator();
            while (sessionIds.hasNext()) {
                SessionID sessionId = (SessionID) sessionIds.next();
                Session.lookupSession(sessionId).logon();
            }
        }
    }

    public void logout() {
        Iterator<SessionID> sessionIds = initiator.getSessions().iterator();
        while (sessionIds.hasNext()) {
            SessionID sessionId = (SessionID) sessionIds.next();
            Session.lookupSession(sessionId).logout("user requested");
        }
    }

    public void stop() {
        
    }
//
//    public JFrame getFrame() {
//        return frame;
//    }

    public static MainMonitor get() {
        return mainMonitor;
    }

    public static void main(String args[]) throws Exception {
        mainMonitor = new MainMonitor(args);
        mainMonitor.startMonitoring();
        mainMonitor.logon();
        Thread.sleep(Long.MAX_VALUE);
    }

	private void startMonitoring() throws Exception {
		loadProperties();
		timer.scheduleAtFixedRate(new TimerTask(){
			public void run(){
				checkAvailability();
			}			
		}, AVAILABILITY_CHECK_INTERVAL_MILLIS, AVAILABILITY_CHECK_INTERVAL_MILLIS);
		
	}
	
	private List<String> monitoredKeys;
	
	//used for debugging on weekends, should not be useful for production runs
	private boolean includeWeekendMonitoring;

	private void checkAvailability() {	
		Calendar c = Calendar.getInstance();
		long t = c.getTimeInMillis();
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		if(!includeWeekendMonitoring  && (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SATURDAY)){
			return;
		}
		c.set(Calendar.HOUR_OF_DAY,0);
		c.set(Calendar.MINUTE,0);
		c.set(Calendar.SECOND,0);
		c.set(Calendar.MILLISECOND,0);
		
		long timeOfDay = t - c.getTimeInMillis();		
		if(timeOfDay<timeOfDayMillisStart || timeOfDay > timeOfDayMillisEnd){
			return;
		}
		List<String> failedKeys = new LinkedList<String>();
		for(String key : monitoredKeys){
			Long lastUpdate = application.lastUpdateTime.get(key);
			if(lastUpdate == null || t-lastUpdate > AVAILABILITY_CHECK_INTERVAL_MILLIS){
				failedKeys.add(key);
			}
		}
		if(!failedKeys.isEmpty()){
			sendDataAvailailityAlert(failedKeys);
		}
	}
	
	private static long parseTimeOfDay(String s){
		StringTokenizer st = new StringTokenizer(s,":");
		long t = 1000*(3600*Integer.parseInt(st.nextToken()) + 60*Integer.parseInt(st.nextToken()) + Integer.parseInt(st.nextToken()));
		log.info("parseTimeOfDay: "+ s+" " + t);
		return t;
	}
	
	private void sendDataAvailailityAlert(List<String> failedKeys) {
		if(failedKeys == null || failedKeys.isEmpty()){
			return;
		}
		log.info("sendDataAvailailityAlert: "+ failedKeys);
		StringBuilder sb = new StringBuilder();
		sb.append(header + "\r\n");
		for(String key : failedKeys){
			sb.append(key).append("\r\n");
		}
		sb.append("\r\n\r\n").append(getHostName()).append("\r\n");
		try {
			EmailSender es = new EmailSender(emailTo, emailSubject, sb.toString());
			es.send();
		} 
		catch (Exception e) {
			log.error("exceptio sending email\r\n" + sb, e);
		}
	}
	
	private volatile String hostName;
	private String getHostName() {
		if(hostName == null){
			try {
				hostName = InetAddress.getLocalHost().getHostName();
			} 
			catch (UnknownHostException e) {
				return ".";
			}
		}
		return hostName;
	}

	private void loadProperties() throws Exception{
		try{
			log.info("loadProperties from : "+ propertiesFile);
			Properties prop = new Properties();
			prop.load(new FileInputStream(propertiesFile));
			log.info("loadProperties from : "+ prop);
			emailTo = prop.getProperty("emailTo", emailTo).trim();
			log.info("loadProperties emailTo: "+ emailTo);
			emailSubject = prop.getProperty("emailSubject", emailSubject);
			log.info("loadProperties emailSubject: "+ emailSubject);
			Properties keyProp = new Properties();
			if(new File(MONITORED_KEYS_OVERRIDE_FILE_NAME).exists()){
				log.info("loadProperties: loading monitored keys from file " + MONITORED_KEYS_OVERRIDE_FILE_NAME);
				InputStream is = new FileInputStream(MONITORED_KEYS_OVERRIDE_FILE_NAME);				
				keyProp.load(is);
				is.close();				
			}
			else{
				log.info("loadProperties: loading monitored keys from resource/jar " + MONITORED_KEYS_RESOURCE_NAME);
				keyProp.load(ClassLoader.class.getResourceAsStream(MONITORED_KEYS_RESOURCE_NAME));
			}
			Set<Object> ks = keyProp.keySet();
			monitoredKeys = new LinkedList<String>();
			for(Object key : ks){
				String s = (""+key).trim();
				if(s.length() > 0){
					monitoredKeys.add(s);
				}
			}
			Collections.sort(monitoredKeys);
			log.info("monitoredKeys: "+ monitoredKeys);
			timeOfDayMillisStart = parseTimeOfDay(prop.getProperty("startTime", START_TIME_DEFAULT));
			timeOfDayMillisEnd = parseTimeOfDay(prop.getProperty("endTime", END_TIME_DEFAULT));
			includeWeekendMonitoring = System.getProperty("MainMonitor.includeWeekendMonitoring", "false").equalsIgnoreCase("true");
			log.info("timeOfDayMillisStart: "+ timeOfDayMillisStart+
				", timeOfDayMillisEnd: " + timeOfDayMillisEnd +
				", includeWeekendMonitoring: " + includeWeekendMonitoring);
		}
		catch(Exception e){
			log.error("exception in loadProperties from : "+ propertiesFile, e);
			throw e;
		}
	}
}	 