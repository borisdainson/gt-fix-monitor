/*******************************************************************************
 * Copyright (c) quickfixengine.org  All rights reserved. 
 * 
 * This file is part of the QuickFIX FIX Engine 
 * 
 * This file may be distributed under the terms of the quickfixengine.org 
 * license as defined by quickfixengine.org and appearing in the file 
 * LICENSE included in the packaging of this file. 
 * 
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
 * THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE. 
 * 
 * See http://www.quickfixengine.org/LICENSE for licensing information. 
 * 
 * Contact ask@quickfixengine.org if any conditions of this licensing 
 * are not clear to you.
 ******************************************************************************/

package com.gt.fix.monitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.gt.fix.monitor.processor.Processor;

import quickfix.Application;
import quickfix.DefaultMessageFactory;
import quickfix.DoNotSend;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.IncorrectDataFormat;
import quickfix.IncorrectTagValue;
import quickfix.Message;
import quickfix.RejectLogon;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionNotFound;
import quickfix.UnsupportedMessageType;
import quickfix.field.AvgPx;
import quickfix.field.BeginString;
import quickfix.field.BusinessRejectReason;
import quickfix.field.ClOrdID;
import quickfix.field.CumQty;
import quickfix.field.DeliverToCompID;
import quickfix.field.ExecID;
import quickfix.field.HandlInst;
import quickfix.field.LastPx;
import quickfix.field.LastShares;
import quickfix.field.LeavesQty;
import quickfix.field.LocateReqd;
import quickfix.field.MDEntryPx;
import quickfix.field.MDEntryType;
import quickfix.field.MDReqID;
import quickfix.field.MarketDepth;
import quickfix.field.MarketID;
import quickfix.field.MsgSeqNum;
import quickfix.field.MsgType;
import quickfix.field.NoMDEntries;
import quickfix.field.OrdStatus;
import quickfix.field.OrdType;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Price;
import quickfix.field.Product;
import quickfix.field.RefMsgType;
import quickfix.field.RefSeqNum;
import quickfix.field.SenderCompID;
import quickfix.field.SessionRejectReason;
import quickfix.field.Side;
import quickfix.field.StopPx;
import quickfix.field.SubscriptionRequestType;
import quickfix.field.Symbol;
import quickfix.field.TargetCompID;
import quickfix.field.Text;
import quickfix.field.TimeInForce;
import quickfix.field.TransactTime;
import quickfix.fix44.MarketDataRequest;

public class MonitorApplication implements Application {
	private Logger log = Logger.getLogger(getClass());
    private DefaultMessageFactory messageFactory = new DefaultMessageFactory();
    private OrderTableModel orderTableModel = null;
    private ExecutionTableModel executionTableModel = null;
    private ObservableOrder observableOrder = new ObservableOrder();
    private ObservableLogon observableLogon = new ObservableLogon();
    private boolean isAvailable = true;
    private boolean isMissingField;
    
    static private TwoWayMap sideMap = new TwoWayMap();
    static private TwoWayMap typeMap = new TwoWayMap();
    static private TwoWayMap tifMap = new TwoWayMap();
    static private HashMap<SessionID, HashSet<ExecID>> execIDs = new HashMap<SessionID, HashSet<ExecID>>();

    //this is used in test runs to collect all keys we care about
    private boolean savesAllFixKeys;
    
    public MonitorApplication(OrderTableModel orderTableModel,
            ExecutionTableModel executionTableModel) {
        this.orderTableModel = orderTableModel;
        this.executionTableModel = executionTableModel;
        savesAllFixKeys = "true".equalsIgnoreCase(System.getProperty("MonitorApplication.savesAllFixKeys"));
    }

    public void onCreate(SessionID sessionID) {
    }

    public void onLogon(SessionID sessionId) {
    	log.info("onLogon: will subscribe");
        observableLogon.logon(sessionId);
        subscribeAll(sessionId);
//        try {
//			subscribeUnsubscribe(sessionId);
//		} 
//        catch (InterruptedException e) {
//			log.error("exception in subscribeUnsubscribe", e);
//		}
    }


	private static final Character TYPE_BID = '0';
	//private Character TYPE_OFFER = '1';
	private static final Character TYPE_ASK = '1';
	//private static final Character TYPE_MID = 'H';//not used? 
	private static final Character TYPE_DV01_USD = 'x';//June 2017 adding 'x' for USD DV01
	private static final Character TYPE_DV01 = 'y';//may 18, 2016 changing DV01 from H to y  'H';//??'y';
	private static final Character TYPE_PV01 = 'z';
	
	
	private static final String[] MARKET_IDS = new String[]{"LCH", "CME"};
	
	
	private void subscribeUnsubscribe(SessionID sessionId) throws InterruptedException {
		String marketId = "LCH";
//		subscribeUnsubscribe(sessionId, 1030, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId);
		subscribeUnsubscribe(sessionId, 21, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId);
		
//		Properties prop = new Properties();
//		try {
//			prop.load(new FileInputStream("../gt-fix-proxy/src/main/resources/topicSegments.properties"));
//		} 
//		catch (IOException e) {
//			throw new RuntimeException(e);
//		}
//		List<Integer> segments = new LinkedList<Integer>();
//		for(Object o : prop.values()){
//			segments.add(new Integer(o.toString()));
//		}
//		Collections.sort(segments);
//		for(Integer segment : segments){
//			subscribeUnsubscribe(sessionId, segment, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId);
//		}
	}
	
    private void subscribeAll(SessionID sessionId) {    
    	for (Enumeration<?> e = marketSegmentProperties.keys(); e.hasMoreElements();) {
            String key = (String)e.nextElement();
            String val = marketSegmentProperties.getProperty(key).trim();
            String[] marketSegment= key.split("\\.");
            if(marketSegment.length != 2){
            	log.error("skipping invalid market-segment sspecification: "+ Arrays.toString(marketSegment) + " " + val);
            	continue;
            }
            String marketId = marketSegment[0].trim();
            int segment = Integer.parseInt(marketSegment[1].trim());
            
            String[] tt = val.split(",");
            char[] types = new char[tt.length];
            for(int i=0; i<tt.length; i++){
            	types[i] = tt[i].trim().charAt(0);
            }
            log.info("subscribeAll: " + key +": " + val + 
            		": " + marketId + " " + segment + 
            		" " + Arrays.toString(types));
            subscribe(sessionId, segment, types, marketId);            
    	}
    	if(true){
    		return;
    	}
    	
    	//DV01 is used for testing but is not necessary for monitoring
    	for(String marketId : MARKET_IDS){
//	   		irs_swap_USD=20
//			fwd_swap_USD=21
//			SB_SwapSwitches_USD=22
//			mac_swap_USD=300
//			imm_swap_USD=401
    		
////yes for DV01 Semi Bond Outrights	6M Fixed 30/360 vs 3M Float ACT/360	20	USDOR[X]	1-30, 35, 40, 45, 50	
////no to Dv01 Semi Bond Switches	6M Fixed 30/360 vs 3M Float ACT/360	22	USDORSW[X]	"Combinations of all following legs:
////    				2-10, 12, 15, 20, 25, 30"	--	Y    		
    		//subscribe(sessionId, 20, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId);    
	   		//subscribe(sessionId, 21, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId); 
	   		//missing in qa: 
    		//subscribe(sessionId, 22, new char[]{TYPE_BID, TYPE_ASK}, marketId);
	   		
//	   		subscribe(sessionId, 300, new char[]{TYPE_BID, TYPE_ASK}, marketId);
//	   		subscribe(sessionId, 401, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, marketId); 
	    }
    	
    	//segment=21, market=LCH
    	subscribe(sessionId, 1300, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	subscribe(sessionId, 2300, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	//subscribe(sessionId, 20, new char[]{TYPE_BID, TYPE_ASK}, "LCH");
    	
    	//unsubscribe(sessionId, 20, new char[]{TYPE_BID, TYPE_ASK}, "CME");
    	
//    	#broken, being fixed 1.2	GBP	Semi Money Outrights vs 6M	6M Fixed ACT/365.FIXED vs 6M Float ACT/365.FIXED	2050
//    	irs_swap_GBP=2050
//
//    	fwd_swap_GBP=2051
//    	imm_swap_GBP=2450
//    	#SB_SwapSwitches_GBP: {}  Not Requested by TX and not into the DB
//    	mac_swap_GBP=2320
    	//works in qa: subscribe(sessionId, 2050, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	//works in qa: subscribe(sessionId, 2051, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	//works in qa:  subscribe(sessionId, 2450, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	//Update symbol 2320 to 2300
    	//works in qa: 
    	//subscribe(sessionId, 2300, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
    	
//    	#EUR_r6M port 14998
//    	proto_mac_swap_EUR=1310
//    	#1.2	EUR	Annual Bond Outrights vs 6M	12M Fixed 30/360 vs 6M Float ACT/360	1030
//    	proto_irs_swap_EUR=1030
//    	proto_fwd_swap_EUR=1031
//    	proto_imm_swap_EUR=1430
    	//We specifically tested EUR segments 
    	//1030, 1031, 1040, 1041, 1310, 1430 and did not receive data for them. 

    	//Update symbol 1310 to 1300
    	//Update symbol 2320 to 2300

//    	if(true){
//	    	//ok inqa: 
//    		subscribe(sessionId, 1030, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//	    	//ok in qa: 
//    		subscribe(sessionId, 1031, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//	    	//ok in qa
//    		subscribe(sessionId, 1300, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//	    	//ok in qa: 
//    		subscribe(sessionId, 1430, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//    	}
//    	if(false){//works in qa 
//    		//irs_swap_EUR-6M=1040
//	    	subscribe(sessionId, 1040, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//	    	//fwd_swap_EUR-6M=1041
//	    	subscribe(sessionId, 1041, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "LCH");
//    	}
    	
    	
    			
    	//#1.2	MXN	Outrights	28D Fixed ACT/360 vs 28D Float ACT/360	11000
    	//irs_swap_MXN=11000
    	//fwd_swap_MXN=11001
    	//imm_swap_MXN=11400
    	if(false){
    	//works in qa:  subscribe(sessionId, 11000, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "CME");
    	//works in qa:  subscribe(sessionId, 11001, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "CME");
    	//works in qa: subscribe(sessionId, 11400, new char[]{TYPE_BID, TYPE_ASK, TYPE_DV01}, "CME");
    	}
	}

	private void subscribe(SessionID sessionId, int segment, char[] types, String marketId) {
		log.info("subscribe: " + sessionId + " " + segment + " " + Arrays.toString(types) + " " + marketId);
        MDReqID mDReqID = new MDReqID("" + System.currentTimeMillis());
        SubscriptionRequestType srt = new SubscriptionRequestType('1'); //1 subscribe, 2 unsubscribe   	         
        MarketDepth md = new MarketDepth(1);
        MarketDataRequest request = new MarketDataRequest(mDReqID, srt, md);
        MarketDataRequest.NoMDEntryTypes noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();
//	        "0 = Bid
//	        1 = Offer
//	        H = Mid-price"
//	      1) Below are the parameters for MAC indicative price feeds over FIX:
//	    	segment: 300, marketID: CME, types: [0, 1]
//	    	segment: 300, marketID: LCH, types: [0, 1]        
        Set<Character> tt = new LinkedHashSet<Character>();
        for(char c : types){
        	tt.add(c);
        }
        for(Character c : tt){
        	noMDEntryTypes.setField(new MDEntryType(c));
        	request.addGroup(noMDEntryTypes);
        }	
        Group noMarketSegments = new Group(1310, MarketID.FIELD);
        noMarketSegments.setField(new MarketID(marketId));
        request.addGroup(noMarketSegments);
        
        MarketDataRequest.NoRelatedSym noRelatedSym = new MarketDataRequest.NoRelatedSym();
//	        
        noRelatedSym.setField(new Symbol("N/A"));
        noRelatedSym.setField(new Product(segment));
        request.addGroup(noRelatedSym);
        
        send(request, sessionId);
	}

	private void subscribeUnsubscribe(SessionID sessionId, int segment, char[] types, String marketId) throws InterruptedException {
		log.info("subscribeUnsubscribe: " + sessionId + " " + segment + " " + Arrays.toString(types) + " " + marketId);
		subscribe(sessionId, segment, types, marketId);
		unsubscribe(sessionId, segment, types, marketId);
	}
	
	private void unsubscribe(SessionID sessionId, int segment, char[] types, String marketId) {
		log.info("unsubscribe: " + sessionId + " " + segment + " " + Arrays.toString(types) + " " + marketId);
        MDReqID mDReqID = new MDReqID("" + System.currentTimeMillis());
        SubscriptionRequestType srt = new SubscriptionRequestType('2'); //1 subscribe, 2 unsubscribe   	         
        MarketDepth md = new MarketDepth(1);
        MarketDataRequest request = new MarketDataRequest(mDReqID, srt, md);
        MarketDataRequest.NoMDEntryTypes noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();     
        Set<Character> tt = new LinkedHashSet<Character>();
        for(char c : types){
        	tt.add(c);
        }
        for(Character c : tt){
        	noMDEntryTypes.setField(new MDEntryType(c));
        	request.addGroup(noMDEntryTypes);
        }
        Group noMarketSegments = new Group(1310, MarketID.FIELD);
        noMarketSegments.setField(new MarketID(marketId));
        request.addGroup(noMarketSegments);
        
        MarketDataRequest.NoRelatedSym noRelatedSym = new MarketDataRequest.NoRelatedSym();
//	        
        noRelatedSym.setField(new Symbol("N/A"));
        noRelatedSym.setField(new Product(segment));
        request.addGroup(noRelatedSym);
        
        send(request, sessionId);
	}
	
	public void onLogout(SessionID sessionID) {
		log.error("onLogout, will call System.exit in 5 sec "+ sessionID);
		System.err.println("onLogout, will call System.exit in 5 sec: "+ sessionID);		
        observableLogon.logoff(sessionID);
        try {
			Thread.sleep(5000);
		} 
        catch (InterruptedException ignore) {
		}
        log.error("onLogout, calling System.exit: "+ sessionID);
        System.err.println("onLogout, calling System.exit: "+ sessionID);
        System.exit(1);
    }

    public void toAdmin(quickfix.Message message, SessionID sessionID) {
    }

    public void toApp(quickfix.Message message, SessionID sessionID) throws DoNotSend {
    	log.info("toApp: "+ message);
    }

    public void fromAdmin(quickfix.Message message, SessionID sessionID) throws FieldNotFound,
            IncorrectDataFormat, IncorrectTagValue, RejectLogon {
    }

    public void fromApp(quickfix.Message message, SessionID sessionId) throws FieldNotFound,
            IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        try {
        	//log.info("fromApp: "+ message.bodyLength());
            //SwingUtilities.invokeLater(new MessageProcessor(message, sessionID));
        	doMessage(message, sessionId);
        } 
        catch (Exception e) {
        	log.error("exception processing message: " + message, e);
        }
    }

    private Set<String> allFixKeys = new HashSet<String>();
	private long messageCounter;
	private Processor processor;
    
    private void doMessage(Message m, SessionID sessionId) throws FieldNotFound {    	
    	messageCounter++; 
    	log.info("doMessage: #" + messageCounter + ": " + m);
    	int priceCounter = 0;
		String marketId = getString(m, MarketID.FIELD);
		Integer segment = getInt(m, Product.FIELD);
		String symbol = getString(m, Symbol.FIELD);
		long t = System.currentTimeMillis();
    	List<Group> groups =  m.getGroups(NoMDEntries.FIELD);
    	//log.info("NoMDEntries.FIELD: "+ groups);
    	Double bid = null;
    	for(Group g : groups){
    		Double value = getDouble(g, MDEntryPx.FIELD);
    		Character type = getChar(g, MDEntryType.FIELD);
    		//log.info("doMessage: " + segment  + " " + marketId + " " + symbol + " " + value + " " + type);
    		String key = marketId + "." + segment + "." + type + "." + symbol;
    		priceCounter++;
    		log.info("doMessage: #" + messageCounter + "."  + priceCounter + ": " + key + " " + value);
    		if(value == null){
    			continue;
    		}
    		if(type == TYPE_BID){
    			bid = value;
    		}
    		else if(type == TYPE_ASK){
    			Double ask = value;
    			if(bid != null && bid > ask){
    				log.error("doMessage: flipped bid>ask: " + key + " " + value +": " + m);
    			}
    		}
    		Long prev = lastUpdateTime.put(key, t);
    		if(log.isDebugEnabled() && prev != null){
    			log.debug("doMessage: dt=" + (t-prev)+ ": " + key);
    		}
		
    		if(savesAllFixKeys && !key.endsWith("15") && allFixKeys.add(key)){
    			saveAllFixkeys(allFixKeys);
    		}
    		
    		if(processor != null){
    			processor.process(marketId, segment, symbol, type, value);
    		}
    	}
    	if(priceCounter == 0){
    		log.info("doMessage: #" + messageCounter + " no prices: " + m);
    	}
    	//log.info("doMessage: #" + messageCounter + " done with "  + priceCounter);
	}

	private static final String FIX_KEYS_FILE_NAME = "monitored_fix_keys.txt";
    
    private void saveAllFixkeys(Set<String> allFixKeys) {
		log.info("saveAllFixkeys: " + allFixKeys);
		List<String> keys = new LinkedList<String>(allFixKeys);
		Collections.sort(keys);
		PrintWriter pw;
		try {
			pw = new PrintWriter(new File(FIX_KEYS_FILE_NAME));
			for(String key : keys){
				pw.println(key);
			}
			pw.close();
		} 
		catch (FileNotFoundException e) {
			log.error("exception saving keys, continue", e);
		}		
	}

	private Character getChar(FieldMap m, int field) {
    	try {
			return m.isSetField(field) ? m.getChar(field) : null;
		} 
		catch (FieldNotFound e) {
			return null;
		}
	}

	private Integer getInt(FieldMap m, int field) {
    	try {
			return m.isSetField(field) ? m.getInt(field) : null;
		} 
		catch (FieldNotFound e) {
			return null;
		}
	}

	private Double getDouble(FieldMap m, int field) {
    	try {
			return m.isSetField(field) ? m.getDouble(field) : null;
		} 
		catch (FieldNotFound e) {
			return null;
		}
	}

	private String getString(FieldMap m, int field){
		try {
			return m.isSetField(field) ? m.getString(field) : null;
		} 
		catch (FieldNotFound e) {
			return null;
		}
	}
	
    private Date getDate(FieldMap m, int field) {
		try {
			return m.isSetField(field) ? m.getUtcTimeStamp(field) : null;
		} 
		catch (FieldNotFound e) {
			return null;
		}
    }


	protected Map<String, Long> lastUpdateTime = new ConcurrentHashMap<String, Long>();
	private Properties marketSegmentProperties;
	
	public class MessageProcessor implements Runnable {
        private quickfix.Message message;
        private SessionID sessionID;

        public MessageProcessor(quickfix.Message message, SessionID sessionID) {
            this.message = message;
            this.sessionID = sessionID;
        }

        public void run() {
            try {
                MsgType msgType = new MsgType();
                if (isAvailable) {
                    if (isMissingField) {
                        // For OpenFIX certification testing
                        sendBusinessReject(message, BusinessRejectReason.CONDITIONALLY_REQUIRED_FIELD_MISSING, "Conditionally required field missing");                        
                    }
                    else if (message.getHeader().isSetField(DeliverToCompID.FIELD)) {
                        // This is here to support OpenFIX certification
                        sendSessionReject(message, SessionRejectReason.COMPID_PROBLEM);
                    } else if (message.getHeader().getField(msgType).valueEquals("8")) {
                        executionReport(message, sessionID);
                    } else if (message.getHeader().getField(msgType).valueEquals("9")) {
                        cancelReject(message, sessionID);
                    } else {
                        sendBusinessReject(message, BusinessRejectReason.UNSUPPORTED_MESSAGE_TYPE,
                                "Unsupported Message Type");
                    }
                } else {
                    sendBusinessReject(message, BusinessRejectReason.APPLICATION_NOT_AVAILABLE,
                            "Application not available");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void sendSessionReject(Message message, int rejectReason) throws FieldNotFound,
            SessionNotFound {
        Message reply = createMessage(message, MsgType.REJECT);
        reverseRoute(message, reply);
        String refSeqNum = message.getHeader().getString(MsgSeqNum.FIELD);
        reply.setString(RefSeqNum.FIELD, refSeqNum);
        reply.setString(RefMsgType.FIELD, message.getHeader().getString(MsgType.FIELD));
        reply.setInt(SessionRejectReason.FIELD, rejectReason);
        Session.sendToTarget(reply);
    }

    private void sendBusinessReject(Message message, int rejectReason, String rejectText)
            throws FieldNotFound, SessionNotFound {
        Message reply = createMessage(message, MsgType.BUSINESS_MESSAGE_REJECT);
        reverseRoute(message, reply);
        String refSeqNum = message.getHeader().getString(MsgSeqNum.FIELD);
        reply.setString(RefSeqNum.FIELD, refSeqNum);
        reply.setString(RefMsgType.FIELD, message.getHeader().getString(MsgType.FIELD));
        reply.setInt(BusinessRejectReason.FIELD, rejectReason);
        reply.setString(Text.FIELD, rejectText);
        Session.sendToTarget(reply);
    }

    private Message createMessage(Message message, String msgType) throws FieldNotFound {
        return messageFactory.create(message.getHeader().getString(BeginString.FIELD), msgType);
    }

    private void reverseRoute(Message message, Message reply) throws FieldNotFound {
        reply.getHeader().setString(SenderCompID.FIELD,
                message.getHeader().getString(TargetCompID.FIELD));
        reply.getHeader().setString(TargetCompID.FIELD,
                message.getHeader().getString(SenderCompID.FIELD));
    }

    private void executionReport(Message message, SessionID sessionID) throws FieldNotFound {

        ExecID execID = (ExecID) message.getField(new ExecID());
        if (alreadyProcessed(execID, sessionID))
            return;

        Order order = orderTableModel.getOrder(message.getField(new ClOrdID()).getValue());
        if (order == null) {
            return;
        }

        BigDecimal fillSize = BigDecimal.ZERO;

        if (message.isSetField(LastShares.FIELD)) {
            LastShares lastShares = new LastShares();
            message.getField(lastShares);
            fillSize = new BigDecimal(""+lastShares.getValue());
        } else {
            // > FIX 4.1
            LeavesQty leavesQty = new LeavesQty();
            message.getField(leavesQty);
            fillSize = new BigDecimal(order.getQuantity()).subtract(new BigDecimal(""+leavesQty.getValue()));
        }

        if (fillSize.compareTo(BigDecimal.ZERO) > 0) {
            order.setOpen(order.getOpen() - (int) Double.parseDouble(fillSize.toPlainString()));
            order.setExecuted(new Integer(message.getString(CumQty.FIELD)));
            order.setAvgPx(new Double(message.getString(AvgPx.FIELD)));
        }
        
        OrdStatus ordStatus = (OrdStatus) message.getField(new OrdStatus());

        if (ordStatus.valueEquals(OrdStatus.REJECTED)) {
            order.setRejected(true);
            order.setOpen(0);
        } else if (ordStatus.valueEquals(OrdStatus.CANCELED)
                || ordStatus.valueEquals(OrdStatus.DONE_FOR_DAY)) {
            order.setCanceled(true);
            order.setOpen(0);
        } else if (ordStatus.valueEquals(OrdStatus.NEW)) {
            if (order.isNew()) {
                order.setNew(false);
            }
        }

        try {
            order.setMessage(message.getField(new Text()).getValue());
        } catch (FieldNotFound e) {
        }

        orderTableModel.updateOrder(order, message.getField(new ClOrdID()).getValue());
        observableOrder.update(order);

        if (fillSize.compareTo(BigDecimal.ZERO) > 0) {
            Execution execution = new Execution();
            execution.setExchangeID(sessionID + message.getField(new ExecID()).getValue());
            
            execution.setSymbol(message.getField(new Symbol()).getValue());
            execution.setQuantity(fillSize.intValue());
            if (message.isSetField(LastPx.FIELD)) {
                execution.setPrice(new Double(message.getString(LastPx.FIELD)));
            }
            Side side = (Side) message.getField(new Side());
            execution.setSide(FIXSideToSide(side));
            executionTableModel.addExecution(execution);
        }
    }

    private void cancelReject(Message message, SessionID sessionID) throws FieldNotFound {

        String id = message.getField(new ClOrdID()).getValue();
        Order order = orderTableModel.getOrder(id);
        if (order == null)
            return;
        if (order.getOriginalID() != null)
            order = orderTableModel.getOrder(order.getOriginalID());

        try {
            order.setMessage(message.getField(new Text()).getValue());
        } catch (FieldNotFound e) {
        }
        orderTableModel.updateOrder(order, message.getField(new OrigClOrdID()).getValue());
    }

    private boolean alreadyProcessed(ExecID execID, SessionID sessionID) {
        HashSet<ExecID> set = execIDs.get(sessionID);
        if (set == null) {
            set = new HashSet<ExecID>();
            set.add(execID);
            execIDs.put(sessionID, set);
            return false;
        } else {
            if (set.contains(execID))
                return true;
            set.add(execID);
            return false;
        }
    }

    private void send(quickfix.Message message, SessionID sessionID) {
        try {
            Session.sendToTarget(message, sessionID);
        } catch (SessionNotFound e) {
            System.out.println(e);
        }
    }

    public void send(Order order) {
//        String beginString = order.getSessionID().getBeginString();
//        if (beginString.equals(FixVersions.BEGINSTRING_FIX40))
//            send40(order);
//        else if (beginString.equals(FixVersions.BEGINSTRING_FIX41))
//            send41(order);
//        else if (beginString.equals(FixVersions.BEGINSTRING_FIX42))
//            send42(order);
//        else if (beginString.equals(FixVersions.BEGINSTRING_FIX43))
//            send43(order);
//        else if (beginString.equals(FixVersions.BEGINSTRING_FIX44))
//            send44(order);
////        else if (beginString.equals(FixVersions.BEGINSTRING_FIXT11))
////            send50(order);
//        else if (beginString.equals(FixVersions.BEGINSTRING_FIXT11))
//            send50sp2(order);        
        send50sp2(order);
    }

//    public void send40(Order order) {
//        quickfix.fix40.NewOrderSingle newOrderSingle = new quickfix.fix40.NewOrderSingle(
//                new ClOrdID(order.getID()), new HandlInst('1'), new Symbol(order.getSymbol()),
//                sideToFIXSide(order.getSide()), new OrderQty(order.getQuantity()),
//                typeToFIXType(order.getType()));
//
//        send(populateOrder(order, newOrderSingle), order.getSessionID());
//    }
//
//    public void send41(Order order) {
//        quickfix.fix41.NewOrderSingle newOrderSingle = new quickfix.fix41.NewOrderSingle(
//                new ClOrdID(order.getID()), new HandlInst('1'), new Symbol(order.getSymbol()),
//                sideToFIXSide(order.getSide()), typeToFIXType(order.getType()));
//        newOrderSingle.set(new OrderQty(order.getQuantity()));
//
//        send(populateOrder(order, newOrderSingle), order.getSessionID());
//    }
//
//    public void send42(Order order) {
//        quickfix.fix42.NewOrderSingle newOrderSingle = new quickfix.fix42.NewOrderSingle(
//                new ClOrdID(order.getID()), new HandlInst('1'), new Symbol(order.getSymbol()),
//                sideToFIXSide(order.getSide()), new TransactTime(), typeToFIXType(order.getType()));
//        newOrderSingle.set(new OrderQty(order.getQuantity()));
//
//        send(populateOrder(order, newOrderSingle), order.getSessionID());
//    }
//
//    public void send43(Order order) {
//        quickfix.fix43.NewOrderSingle newOrderSingle = new quickfix.fix43.NewOrderSingle(
//                new ClOrdID(order.getID()), new HandlInst('1'), sideToFIXSide(order.getSide()),
//                new TransactTime(), typeToFIXType(order.getType()));
//        newOrderSingle.set(new OrderQty(order.getQuantity()));
//        newOrderSingle.set(new Symbol(order.getSymbol()));
//        send(populateOrder(order, newOrderSingle), order.getSessionID());
//    }
//
//    public void send44(Order order) {
//        quickfix.fix44.NewOrderSingle newOrderSingle = new quickfix.fix44.NewOrderSingle(
//                new ClOrdID(order.getID()), sideToFIXSide(order.getSide()),
//                new TransactTime(), typeToFIXType(order.getType()));
//        newOrderSingle.set(new OrderQty(order.getQuantity()));
//        newOrderSingle.set(new Symbol(order.getSymbol()));
//        newOrderSingle.set(new HandlInst('1'));
//        MDReqID mDReqID = new MDReqID("" + System.currentTimeMillis());
//        SubscriptionRequestType srt = new SubscriptionRequestType( 
//        		order.getSide() == OrderSide.BUY ? '1' : '2'); //1 subscribe, 2 unsubscribe   
//         
//        MarketDepth md = new MarketDepth(1);
//        MarketDataRequest request = new MarketDataRequest(mDReqID, srt, md);
//        MarketDataRequest.NoMDEntryTypes noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();
////        "0 = Bid
////        1 = Offer
////        H = Mid-price"
//        noMDEntryTypes.setField(new MDEntryType('0'));
//        request.addGroup(noMDEntryTypes);
//        noMDEntryTypes.setField(new MDEntryType('1'));
//        request.addGroup(noMDEntryTypes);
//        noMDEntryTypes.setField(new MDEntryType(TYPE_DV01));
//        request.addGroup(noMDEntryTypes);
//        noMDEntryTypes.setField(new MDEntryType('b'));
//        request.addGroup(noMDEntryTypes);
//                
//        MarketDataRequest.NoRelatedSym noRelatedSym = new MarketDataRequest.NoRelatedSym();
////        20 USD semi-bond vs 3M LIBOR outrights (whole year tenors, 1 through 50)
////        22  USD semi-bond vs 3M LIBOR switches (tenors 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30)
////        23  USD semi-bond vs 3M LIBOR butterflies (tenors 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30)
////        300  USD MAC outrights (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
////        302  USD MAC rolls (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
////        303  USD MAC switches (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
////        304  USD MAC butterflies (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
////        530  EUR Annual bond vs 3M EURIBOR (EUR-EURIBOR-Reuters) outright (whole year tenors, 1 through 30) 
////        540  EUR Annual bond vs 6M EURIBOR (EUR-EURIBOR-Reuters) outright (whole year tenors, 1 through 30)
////        <field number="460" name="Product" type="INT">
////        <value enum="1" description="AGENCY"/>
////        <value enum="2" description="COMMODITY"/>
////        <value enum="3" description="CORPORATE"/>
////        <value enum="4" description="CURRENCY"/>
////        <value enum="5" description="EQUITY"/>
////        <value enum="6" description="GOVERNMENT"/>
////        <value enum="7" description="INDEX"/>
////        <value enum="8" description="LOAN"/>
////        <value enum="9" description="MONEYMARKET"/>
////        <value enum="10" description="MORTGAGE"/>
////        <value enum="11" description="MUNICIPAL"/>
////        <value enum="12" description="OTHER"/>
////        <value enum="13" description="FINANCING"/>
////	      </field>
//        noRelatedSym.setField(new Symbol("N/A"));
//        noRelatedSym.setField(new Product(order.getQuantity()));
//        request.addGroup(noRelatedSym);
//        
//        boolean addSymbolSubscription = false;
//        if(addSymbolSubscription){
//	        noRelatedSym.setField(new Symbol(order.getSymbol()));
//	        noRelatedSym.removeField(Product.FIELD);
//	        request.addGroup(noRelatedSym);
//        }
//        send(request, order.getSessionID());
//    }

    public void send50sp2(Order order) {
        quickfix.fix50sp2.NewOrderSingle newOrderSingle = new quickfix.fix50sp2.NewOrderSingle(
                new ClOrdID(order.getID()), sideToFIXSide(order.getSide()),
                new TransactTime(), typeToFIXType(order.getType()));
        newOrderSingle.set(new OrderQty(order.getQuantity()));
        
        newOrderSingle.set(new Symbol(order.getSymbol()));
        
        newOrderSingle.set(new HandlInst('1'));
        MDReqID mDReqID = new MDReqID("" + System.currentTimeMillis());
        SubscriptionRequestType srt = new SubscriptionRequestType( 
        		order.getSide() == OrderSide.BUY ? '1' : '2'); //1 subscribe, 2 unsubscribe   
         
        MarketDepth md = new MarketDepth(1);
        MarketDataRequest request = new MarketDataRequest(mDReqID, srt, md);
        MarketDataRequest.NoMDEntryTypes noMDEntryTypes = new MarketDataRequest.NoMDEntryTypes();
//        "0 = Bid
//        1 = Offer
//        H = Mid-price"
//          y = DV01 
//      1) Below are the parameters for MAC indicative price feeds over FIX:
//    	segment: 300, marketID: CME, types: [0, 1]
//    	segment: 300, marketID: LCH, types: [0, 1]        
        noMDEntryTypes.setField(new MDEntryType(TYPE_BID));
        request.addGroup(noMDEntryTypes);
        noMDEntryTypes.setField(new MDEntryType(TYPE_ASK));
        request.addGroup(noMDEntryTypes);        
        if(false){
        	noMDEntryTypes.setField(new MDEntryType(TYPE_DV01));
        	request.addGroup(noMDEntryTypes);
        }
//        noMDEntryTypes.setField(new MDEntryType('z'));
//        request.addGroup(noMDEntryTypes);
//        
//      1310	NoMarketSegments		<integer>	N	When provided, always 1	Requried when 263=1
//		->	1301	MarketID	<string>	N	"CME = Request for CME data
//		LCH = Request for LCH data"	
        Group noMarketSegments = new Group(1310, MarketID.FIELD);
        noMarketSegments.setField(new MarketID(order.getSymbol()));
        request.addGroup(noMarketSegments);
        
        MarketDataRequest.NoRelatedSym noRelatedSym = new MarketDataRequest.NoRelatedSym();
//        20 USD semi-bond vs 3M LIBOR outrights (whole year tenors, 1 through 50)
//        22  USD semi-bond vs 3M LIBOR switches (tenors 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30)
//        23  USD semi-bond vs 3M LIBOR butterflies (tenors 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 20, 25, 30)
//        300  USD MAC outrights (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
//        302  USD MAC rolls (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
//        303  USD MAC switches (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
//        304  USD MAC butterflies (tenors 1, 2, 3, 4, 5, 7, 9, 10, 12, 15, 20, 30)
//        530  EUR Annual bond vs 3M EURIBOR (EUR-EURIBOR-Reuters) outright (whole year tenors, 1 through 30) 
//        540  EUR Annual bond vs 6M EURIBOR (EUR-EURIBOR-Reuters) outright (whole year tenors, 1 through 30)
//        <field number="460" name="Product" type="INT">
//        <value enum="1" description="AGENCY"/>
//        <value enum="2" description="COMMODITY"/>
//        <value enum="3" description="CORPORATE"/>
//        <value enum="4" description="CURRENCY"/>
//        <value enum="5" description="EQUITY"/>
//        <value enum="6" description="GOVERNMENT"/>
//        <value enum="7" description="INDEX"/>
//        <value enum="8" description="LOAN"/>
//        <value enum="9" description="MONEYMARKET"/>
//        <value enum="10" description="MORTGAGE"/>
//        <value enum="11" description="MUNICIPAL"/>
//        <value enum="12" description="OTHER"/>
//        <value enum="13" description="FINANCING"/>
//	      </field>
        
        
        noRelatedSym.setField(new Symbol("N/A"));
        noRelatedSym.setField(new Product(order.getQuantity()));
        request.addGroup(noRelatedSym);
        
        //TODO: add symbols for symbol-specific subscriptions 
        //noRelatedSym.setField(new Symbol(order.getSymbol()));
        if(false){
	        noRelatedSym.removeField(Product.FIELD);
	        request.addGroup(noRelatedSym);
        }
        send(request, order.getSessionID());
    }

//    public void send50sp2(Order order) {
//        quickfix.fix50sp2.NewOrderSingle newOrderSingle = new quickfix.fix50sp2.NewOrderSingle(
//                new ClOrdID(order.getID()), sideToFIXSide(order.getSide()),
//                new TransactTime(), typeToFIXType(order.getType()));
//        newOrderSingle.set(new OrderQty(order.getQuantity()));
//        newOrderSingle.set(new Symbol(order.getSymbol()));
//        newOrderSingle.set(new HandlInst('1'));
//        send(populateOrder(order, newOrderSingle), order.getSessionID());
//    }

    public quickfix.Message populateOrder(Order order, quickfix.Message newOrderSingle) {

        OrderType type = order.getType();

        if (type == OrderType.LIMIT)
            newOrderSingle.setField(new Price(order.getLimit().doubleValue()));
        else if (type == OrderType.STOP) {
            newOrderSingle.setField(new StopPx(order.getStop().doubleValue()));
        } else if (type == OrderType.STOP_LIMIT) {
            newOrderSingle.setField(new Price(order.getLimit().doubleValue()));
            newOrderSingle.setField(new StopPx(order.getStop().doubleValue()));
        }

        if (order.getSide() == OrderSide.SHORT_SELL
                || order.getSide() == OrderSide.SHORT_SELL_EXEMPT) {
            newOrderSingle.setField(new LocateReqd(false));
        }

        newOrderSingle.setField(tifToFIXTif(order.getTIF()));
        return newOrderSingle;
    }

    public void cancel(Order order) {
//        String beginString = order.getSessionID().getBeginString();
//        if (beginString.equals("FIX.4.0"))
//            cancel40(order);
//        else if (beginString.equals("FIX.4.1"))
//            cancel41(order);
//        else if (beginString.equals("FIX.4.2"))
//            cancel42(order);
//        return;
    	throw new UnsupportedOperationException("cancel");
    }

//    public void cancel40(Order order) {
//        String id = order.generateID();
//        quickfix.fix40.OrderCancelRequest message = new quickfix.fix40.OrderCancelRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(id), new CxlType(CxlType.FULL_REMAINING_QUANTITY), new Symbol(order
//                        .getSymbol()), sideToFIXSide(order.getSide()), new OrderQty(order
//                        .getQuantity()));
//
//        orderTableModel.addID(order, id);
//        send(message, order.getSessionID());
//    }
//
//    public void cancel41(Order order) {
//        String id = order.generateID();
//        quickfix.fix41.OrderCancelRequest message = new quickfix.fix41.OrderCancelRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(id), new Symbol(order.getSymbol()),
//                sideToFIXSide(order.getSide()));
//        message.setField(new OrderQty(order.getQuantity()));
//
//        orderTableModel.addID(order, id);
//        send(message, order.getSessionID());
//    }
//
//    public void cancel42(Order order) {
//        String id = order.generateID();
//        quickfix.fix42.OrderCancelRequest message = new quickfix.fix42.OrderCancelRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(id), new Symbol(order.getSymbol()),
//                sideToFIXSide(order.getSide()), new TransactTime());
//        message.setField(new OrderQty(order.getQuantity()));
//
//        orderTableModel.addID(order, id);
//        send(message, order.getSessionID());
//    }

    public void replace(Order order, Order newOrder) {
//        String beginString = order.getSessionID().getBeginString();
//        if (beginString.equals("FIX.4.0"))
//            replace40(order, newOrder);
//        else if (beginString.equals("FIX.4.1"))
//            replace41(order, newOrder);
//        else if (beginString.equals("FIX.4.2"))
//            replace42(order, newOrder);
//        return;
        throw new UnsupportedOperationException("replace");
    }

//    public void replace40(Order order, Order newOrder) {
//        quickfix.fix40.OrderCancelReplaceRequest message = new quickfix.fix40.OrderCancelReplaceRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(newOrder.getID()), new HandlInst('1'),
//                new Symbol(order.getSymbol()), sideToFIXSide(order.getSide()), new OrderQty(
//                        newOrder.getQuantity()), typeToFIXType(order.getType()));
//
//        orderTableModel.addID(order, newOrder.getID());
//        send(populateCancelReplace(order, newOrder, message), order.getSessionID());
//    }
//
//    public void replace41(Order order, Order newOrder) {
//        quickfix.fix41.OrderCancelReplaceRequest message = new quickfix.fix41.OrderCancelReplaceRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(newOrder.getID()), new HandlInst('1'),
//                new Symbol(order.getSymbol()), sideToFIXSide(order.getSide()), typeToFIXType(order
//                        .getType()));
//
//        orderTableModel.addID(order, newOrder.getID());
//        send(populateCancelReplace(order, newOrder, message), order.getSessionID());
//    }
//
//    public void replace42(Order order, Order newOrder) {
//        quickfix.fix42.OrderCancelReplaceRequest message = new quickfix.fix42.OrderCancelReplaceRequest(
//                new OrigClOrdID(order.getID()), new ClOrdID(newOrder.getID()), new HandlInst('1'),
//                new Symbol(order.getSymbol()), sideToFIXSide(order.getSide()), new TransactTime(),
//                typeToFIXType(order.getType()));
//
//        orderTableModel.addID(order, newOrder.getID());
//        send(populateCancelReplace(order, newOrder, message), order.getSessionID());
//    }
//
//    Message populateCancelReplace(Order order, Order newOrder, quickfix.Message message) {
//
//        if (order.getQuantity() != newOrder.getQuantity())
//            message.setField(new OrderQty(newOrder.getQuantity()));
//        if (!order.getLimit().equals(newOrder.getLimit()))
//            message.setField(new Price(newOrder.getLimit().doubleValue()));
//        return message;
//    }

    public Side sideToFIXSide(OrderSide side) {
        return (Side) sideMap.getFirst(side);
    }

    public OrderSide FIXSideToSide(Side side) {
        return (OrderSide) sideMap.getSecond(side);
    }

    public OrdType typeToFIXType(OrderType type) {
        return (OrdType) typeMap.getFirst(type);
    }

    public OrderType FIXTypeToType(OrdType type) {
        return (OrderType) typeMap.getSecond(type);
    }

    public TimeInForce tifToFIXTif(OrderTIF tif) {
        return (TimeInForce) tifMap.getFirst(tif);
    }

    public OrderTIF FIXTifToTif(TimeInForce tif) {
        return (OrderTIF) typeMap.getSecond(tif);
    }

    public void addLogonObserver(Observer observer) {
        observableLogon.addObserver(observer);
    }

    public void deleteLogonObserver(Observer observer) {
        observableLogon.deleteObserver(observer);
    }

    public void addOrderObserver(Observer observer) {
        observableOrder.addObserver(observer);
    }

    public void deleteOrderObserver(Observer observer) {
        observableOrder.deleteObserver(observer);
    }

    private static class ObservableOrder extends Observable {
        public void update(Order order) {
            setChanged();
            notifyObservers(order);
            clearChanged();
        }
    }

    private static class ObservableLogon extends Observable {
        private HashSet<SessionID> set = new HashSet<SessionID>();

        public void logon(SessionID sessionID) {
            set.add(sessionID);
            setChanged();
            notifyObservers(new LogonEvent(sessionID, true));
            clearChanged();
        }

        public void logoff(SessionID sessionID) {
            set.remove(sessionID);
            setChanged();
            notifyObservers(new LogonEvent(sessionID, false));
            clearChanged();
        }
    }

    static {
        sideMap.put(OrderSide.BUY, new Side(Side.BUY));
        sideMap.put(OrderSide.SELL, new Side(Side.SELL));
        sideMap.put(OrderSide.SHORT_SELL, new Side(Side.SELL_SHORT));
        sideMap.put(OrderSide.SHORT_SELL_EXEMPT, new Side(Side.SELL_SHORT_EXEMPT));
        sideMap.put(OrderSide.CROSS, new Side(Side.CROSS));
        sideMap.put(OrderSide.CROSS_SHORT, new Side(Side.CROSS_SHORT));

        typeMap.put(OrderType.MARKET, new OrdType(OrdType.MARKET));
        typeMap.put(OrderType.LIMIT, new OrdType(OrdType.LIMIT));
        typeMap.put(OrderType.STOP, new OrdType(OrdType.STOP));
        typeMap.put(OrderType.STOP_LIMIT, new OrdType(OrdType.STOP_LIMIT));

        tifMap.put(OrderTIF.DAY, new TimeInForce(TimeInForce.DAY));
        tifMap.put(OrderTIF.IOC, new TimeInForce(TimeInForce.IMMEDIATE_OR_CANCEL));
        tifMap.put(OrderTIF.OPG, new TimeInForce(TimeInForce.AT_THE_OPENING));
        tifMap.put(OrderTIF.GTC, new TimeInForce(TimeInForce.GOOD_TILL_CANCEL));
        tifMap.put(OrderTIF.GTX, new TimeInForce(TimeInForce.GOOD_TILL_CROSSING));

    }
    
    public boolean isMissingField() {
        return isMissingField;
    }
    
    public void setMissingField(boolean isMissingField) {
        this.isMissingField = isMissingField;
    }

    public boolean isAvailable() {
        return isAvailable;
    }
    
    public void setAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

	public void setMarketSegmentProperties(Properties marketSegmentProperties) {
		this.marketSegmentProperties = marketSegmentProperties;
	}
}
