package com.gt.fix.monitor.processor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.Timer;

import org.apache.log4j.Logger;

public class ChangeFromPreviousMonitor implements Processor{
	private static final String START_TIME_DEFAULT = "02:00:00";
	private static final String END_TIME_DEFAULT = "19:00:00";
	private Set<String> monitoredRics = new HashSet<>();
	private Map<String, Double> previousValues = new HashMap<>();
	private double threshold = 0.005;
	private long historyLengthMillis = 60*1000;
	private String emailTo = "boris.dainson@gmail.com";
	private String emailSubject = "Alert from RealTimeClient";
	private static Logger log = Logger.getLogger(ChangeFromPreviousMonitor.class);
	private Timer timer = new Timer(true);
	private int timerTaskInterval = 5*60*1000;
	private String propertiesFile = "conf/monitoring.txt";
	private long timeOfDayMillisStart = 0L; //parseTimeOfDay("6:30:00");
	private long timeOfDayMillisEnd = Long.MAX_VALUE; //parseTimeOfDay("19:00:00");
	private String header = "Large basis feed variations";

	
	
	public ChangeFromPreviousMonitor(){
//		monitoredRics.addAll(Arrays.asList(null));
//		timer.schedule(new TimerTask(){
//			@Override
//			public void run() {
//				sendAlerts();
//				checkForNewPropertiesFile();
//			}			
//		}, timerTaskInterval, timerTaskInterval);
//		try{
//			propertiesFileLastModified = System.currentTimeMillis();
//			loadProperties();
//		}
//		catch(Exception e){
//			log.error("initialization exception, continue", e);
//		}
	}
	
//	private long propertiesFileLastModified;
//	private void checkForNewPropertiesFile() {
//		File f = new File(propertiesFile);
//		if(f.exists() && f.lastModified() > propertiesFileLastModified){
//			log.warn("checkForNewPropertiesFile: file changed, will reload. LastModified " + new Date(f.lastModified()));
//			propertiesFileLastModified = f.lastModified();
//			loadProperties();
//		}
//	}

//	private static long parseTimeOfDay(String s){
//		StringTokenizer st = new StringTokenizer(s,":");
//		long t = 1000*(3600*Integer.parseInt(st.nextToken()) + 60*Integer.parseInt(st.nextToken()) + Integer.parseInt(st.nextToken()));
//		log.info("parseTimeOfDay: "+ s+" " + t);
//		return t;
//	}
//
//	private void sendAlerts() {
//		List<String> messages;
//		synchronized (notificationsByRic) {
//			messages = new ArrayList<String>(notificationsByRic.values());
//			notificationsByRic.clear();
//		}
//		if(messages.size() == 0){
//			return;
//		}
//		Collections.sort(messages);
//		StringBuilder sb = new StringBuilder();
//		sb.append(header + "\r\n");
//		for(String message : messages){
//			sb.append(message).append("\r\n");
//		}
//		sb.append("\r\n\r\n").append(getHostName()).append("\r\n");
//		try {
//			EmailSender es = new EmailSender(emailTo, emailSubject, sb.toString());
//			es.send();
//		} 
//		catch (Exception e) {
//			log.error("exceptio sending email\r\n" + sb, e);
//		}
//	}
//
//	private volatile String hostName;
//	private String getHostName() {
//		if(hostName == null){
//			try {
//				hostName = InetAddress.getLocalHost().getHostName();
//			} 
//			catch (UnknownHostException e) {
//				return ".";
//			}
//		}
//		return hostName;
//	}
//
//	public void stop(){
//		timer.cancel();
//		timer = null;
//	}
//	
//	public boolean process(Quote eq) {
//		if(eq == null || eq.bid == Double.NaN || eq.ask == Double.NaN || eq.ric == null ||  !monitoredRics.contains(eq.ric)){
//			return true;
//		}
//		Calendar c = Calendar.getInstance();
//		long t = c.getTimeInMillis();
//		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
//		if(dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SATURDAY){
//			return true;
//		}
//		c.set(Calendar.HOUR_OF_DAY,0);
//		c.set(Calendar.MINUTE,0);
//		c.set(Calendar.SECOND,0);
//		c.set(Calendar.MILLISECOND,0);
//		
//		long timeOfDay = t- c.getTimeInMillis();
//		
//		if(timeOfDay<timeOfDayMillisStart || timeOfDay > timeOfDayMillisEnd){
//			return true;
//		}
//		LinkedList<QuoteMid> recent = recentQuotes.get(eq.ric);
//		if(recent == null){
//			recent = new LinkedList<>();
//			recent.add(new QuoteMid(eq));
//			recentQuotes.put(eq.ric, recent);
//			return true;
//		}
//		QuoteMid previous = recent.getLast();
//		QuoteMid q = new QuoteMid(eq);
//		if(!isSameDay(eq, previous)){
//			recent.clear();
//			recent.add(q);
//			log.info("skip previous day, purge history: " + previous);
//			return true;
//		}
//		if(q.feedTime - previous.feedTime > this.historyLengthMillis){
//			if(Math.abs(q.mid - previous.mid) >= threshold){
//				addNotification(q, previous);
//			}
//			recent.clear();
//			recent.add(q);
//			return true;
//		}
//		for(QuoteMid qm : recent){
//			if(q.feedTime - qm.feedTime < this.historyLengthMillis){
//				if(Math.abs(q.mid - previous.mid) >= threshold){
//					addNotification(q, previous);
//					break;
//				}
//			}
//		}
//		recent.add(q);
//		while(recent.size() > 1 && q.feedTime - recent.getFirst().feedTime > this.historyLengthMillis){
//			recent.removeFirst();					
//		}
//		return true;
//	}
//	
//	private Map<String, String> notificationsByRic = new HashMap<String, String>(); 
//	
//	private void addNotification(QuoteMid q, QuoteMid previous) {		
//		String message = q.ric + " from " + previous.mid + " to " + q.mid;
//		log.info("addNotification: "+ message);
//		synchronized (notificationsByRic) {
//			notificationsByRic.put(q.ric, message);
//		}
//	}
//
//	private boolean isSameDay(Quote eq, QuoteMid previous) {
//		Calendar c = Calendar.getInstance();
//		c.setTimeInMillis(eq.feedTime);
//		int d1 = c.get(Calendar.YEAR)*1000 + c.get(Calendar.DAY_OF_YEAR);
//		
//		c.setTimeInMillis(previous.feedTime);
//		int d2 = c.get(Calendar.YEAR)*1000 + c.get(Calendar.DAY_OF_YEAR);
//		
//		return d1 == d2;
//	}
//
//	private class QuoteMid{
//		String ric;
//		long feedTime;
//		double mid;
//		QuoteMid(Quote q){
//			this.ric = q.ric;
//			this.feedTime = q.feedTime;
//			this.mid = (q.bid + q.ask)/2.0;
//		}
//		
//		@Override
//		public String toString() {
//			return "QuoteMid [ric=" + ric + ", feedTime=" + new Date(feedTime) + ", mid="
//					+ mid + "]";
//		}
//	}
	
//	public void setMonitoredRics(List<String> rics){
//		monitoredRics = new HashSet<>(rics);
//	}
//	
//	public void setPropertiesFile(String propertiesFile) {
//		this.propertiesFile = propertiesFile;
//		loadProperties();
//	}

//	private void loadProperties() {
//		try{
//			log.info("loadProperties from : "+ propertiesFile);
//			Properties prop = new Properties();
//			prop.load(new FileInputStream(propertiesFile));
//			log.info("loadProperties from : "+ prop);
//			emailTo = prop.getProperty("emailTo", emailTo).trim();
//			log.info("loadProperties emailTo: "+ emailTo);
//			emailSubject = prop.getProperty("emailSubject", emailSubject);
//			log.info("loadProperties emailSubject: "+ emailSubject);
//			String rics = prop.getProperty("rics", "").trim();
//			if(rics.length() > 0){				
//				monitoredRics.clear();
//				for(String ric : rics.split(",")){
//					monitoredRics.add(ric.trim());	
//				}				
//			}
//			log.info("loadProperties monitoredRics: "+ monitoredRics);
//			threshold = Double.parseDouble(prop.getProperty("threshold",""+threshold).trim());
//			historyLengthMillis = 1000L * Long.parseLong(prop.getProperty("historyLengthSeconds", ""+historyLengthMillis/1000).trim()) ;
//			timeOfDayMillisStart = parseTimeOfDay(prop.getProperty("startTime", START_TIME_DEFAULT));
//			timeOfDayMillisEnd = parseTimeOfDay(prop.getProperty("endTime", END_TIME_DEFAULT));
//			log.info("threshold: "+ threshold +", historyLengthMillis: "+ historyLengthMillis+
//				", timeOfDayMillisStart: "+ timeOfDayMillisStart+
//				", timeOfDayMillisEnd: " + timeOfDayMillisEnd);
//		}
//		catch(Exception e){
//			log.error("exception in loadProperties from : "+ propertiesFile, e);
//		}
//	}
	
	public static void main(String[] args) throws InterruptedException{
		new ChangeFromPreviousMonitor();
		Thread.sleep(Long.MAX_VALUE);
	}

	@Override
	public boolean process(String marketId, Integer segment, String symbol,
			Character type, Double value) {
		// TODO Auto-generated method stub
		return false;
	}
}
