package com.gt.fix.monitor.processor;

public interface Processor {
	boolean process(String marketId, Integer segment, String symbol, Character type, Double value);
}
